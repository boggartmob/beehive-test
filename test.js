const debounce = require('./debounce.js');

// Testing setup
const Mocha = require('mocha');
const assert = require('assert');
const sinon = require('sinon');
const mocha = new Mocha();
const DEFAULT_DELAY = 100;
mocha.suite.emit('pre-require', this, 'solution', mocha);
// helper function
const sleep = (ms = 0) => new Promise(resolve => setTimeout(resolve, ms));
describe('debounce', () => {
    let spy;
    let dFunc;
    beforeEach(() => {
        spy = sinon.spy();
        dFunc = debounce(spy, DEFAULT_DELAY);
    });
    it('should not invoke before debounce delay', () => {
        dFunc();
        assert(spy.notCalled);
    });
    it('should invoke after debounce delay', async () => {
        dFunc();
        await sleep(DEFAULT_DELAY);
        assert(spy.calledOnce);
    });
    it('should debounce multiple invoke and only invoke once', async () => {
        // first invoke
        dFunc();
        await sleep(DEFAULT_DELAY-(DEFAULT_DELAY/4)*3);
        // second invoke
        dFunc();
        await sleep(DEFAULT_DELAY-DEFAULT_DELAY/4);
        // after 500ms
        assert(spy.notCalled);
        // after 1000ms
        await sleep(DEFAULT_DELAY);
        assert(spy.calledOnce);
    });
    it('should debounce and invoke with args given', async () => {
        const mockArgs = 'mockArgs';
        dFunc(mockArgs);
        await sleep(DEFAULT_DELAY);
        assert(spy.withArgs(mockArgs).calledOnce);
    });

    it('should debounce and invoke again after delay', async () => {
        const mockArgs = 'mockArgs';
        dFunc(mockArgs);
        await sleep(DEFAULT_DELAY);
        assert(spy.withArgs(mockArgs).calledOnce);

        dFunc(mockArgs);
        await sleep(DEFAULT_DELAY);
        assert(spy.withArgs(mockArgs).calledTwice);

        const newArgs = 'newArgs';
        dFunc(newArgs);

        await sleep(DEFAULT_DELAY);
        assert(spy.withArgs(newArgs).calledOnce);

        assert(spy.callCount === 3);
    });
});

// exec tests
mocha.run();