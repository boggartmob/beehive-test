# README #

Debounce decorator code located in `debounce.js` file.

One more test added to make sure debounced function could be called again after first successful call with the same or new arguments.

To run tests use following commands:

~~~
git clone https://bitbucket.org/boggartmob/beehive-test.git
cd ./beehive-test
npm i
npm test
~~~

Node.js should be installed.

https://nodejs.org/en/

---

*Debounce and Throttle are commonly used techniques in web development, please describe the difference between them.*

Debounce is a way to invoke actual call after defined time after last real call. Useful when we have a lot of calls like populating input field. We do not want to invoke actual call for each symbol user placed into field so wait few moments and if user stopped to type - invoke function to process whole input. Visualization example could be some plot interaction when user add/remove something on the plot but we hit backend only when he stopped for some time. Another example - zooming into low resolution data. During scroll events we can update viz current data. After user stopped to scroll - load/draw data in high resolution to show more details. 

Throttling provides pretty the same functionality but invoke function after defined time not depending on last call time. In other words we dont reset timeout each new call, just ignore them. It allows reduce number of actual calls, skip calls we don't need, aggregate data for defined period of time. Can be used in visualizations to update selection rect for example not every mouse event but if pointer moved enough, in logging to cache events and send them batch.