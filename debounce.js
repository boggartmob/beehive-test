const debounce = (func, delay) => {
    let timeoutId = null;

    return (...args) => {
        clearTimeout(timeoutId);

        timeoutId = setTimeout(() => {
            func.apply(null, args);
        }, delay);
    };
};

module.exports = debounce;